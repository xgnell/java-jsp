/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package blackghost.sample.jsp;

/**
 *
 * @author blackghost
 */
public class Student {
	private String name;
	private Integer age;
	private Boolean isMonitor;
	
	public final String getName() {
		return this.name;
	}
	public final void setName(final String name) {
		this.name = name;
	}
	
	public final Integer getAge() {
		return this.age;
	}
	public final void setAge(final Integer age) {
		this.age = age;
	}
	
	public final Boolean getIsMonitor()
	{
		return this.isMonitor;
	}
	public final void setIsMonitor(final Boolean isMonitor)
	{
		this.isMonitor = isMonitor;
	}
	
	public Student() { super(); }
	public Student(final String name, final Integer age, final Boolean isMonitor) {
		super();
		setName(name);
		setAge(age);
		setIsMonitor(isMonitor);
	}
	
	@Override
	public String toString() {
		return "Name: " + name + ", Age: " + age;
	}
}

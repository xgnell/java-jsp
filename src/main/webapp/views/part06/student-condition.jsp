<%-- 
    Document   : student-condition
    Created on : Nov 4, 2021, 6:43:49 PM
    Author     : blackghost
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"
	import="java.util.*, blackghost.sample.jsp.Student" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
		<style>
			.display-table
			{
				border: 1px black solid;
				border-collapse: collapse;
				text-align: center;
			}
			
			.display-table th
			{
				border: 1px black solid;
				padding: 5px 5px 5px 5px;
				background-color: blue;
				color: white;
			}
			
			.display-table tr td
			{
				border: 1px black solid;
				padding: 5px 5px 5px 5px;
			}
			
		</style>
    </head>
    <body>
		<%
			Student[] students = new Student[] {
				new Student("Nam", 12, true),
				new Student("Hoa", 13, false),
				new Student("Tuan", 17, false),
				new Student("Manh", 18, true),
				new Student("Linh", 20, false),
			};
			
			pageContext.setAttribute("students", students);
		%>
		
        <h1>Students</h1>
		
		<table class="display-table">
			<tr>
				<th>Name</th>
				<th>Age</th>
				<th>Is monitor ?</th>
			</tr>
			
			<c:forEach var="student" items="${students}">
				<tr>
					<td>${student.name}</td>
					<td>${student.age}</td>
					<c:choose>
						<c:when test="${student.isMonitor}">
							<td>x</td>
						</c:when>
						<c:otherwise>
							<td>.</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
		</table>
    </body>
</html>

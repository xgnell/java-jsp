<%-- 
    Document   : foreach.jsp
    Created on : Nov 2, 2021, 8:47:57 PM
    Author     : blackghost
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
		<%
			int[] xs = new int[] { 1, 2, 3, 4, 5 };
			pageContext.setAttribute("xs", xs);
		%>
        <h1>Simple foreach</h1>
		<c:forEach var="x" items="${xs}">
			<p>${x}</p>
		</c:forEach>
    </body>
</html>

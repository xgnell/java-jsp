<%-- 
    Document   : cookies-form
    Created on : Nov 2, 2021, 7:05:45 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Developer Languages News</title>
		<style>
			.center
			{
				text-align: center;
			}
			
			.display-form
			{
				display: flex;
				justify-content: center;
			}
			
			.display-form form
			{
				padding: 15px 15px 15px 15px;
				border: 1px black solid;
			}
		</style>
    </head>
    <body>
        <h1 class="center">Update your language</h1>
		<div class="display-form">
			<form method="GET" action="cookies-form-response.jsp">
				Your favourite language:
				<select name="language">
					<option value="C++">C++</option>
					<option value="Java">Java</option>
					<option value="C#">C#</option>
					<option value="Python">Python</option>
					<option value="Javascript">Javascript</option>
					<option value="Brain fuck">Brain fuck</option>
				</select>
				<br>
				<input type="submit" value="Send">
			</form>
		</div>
    </body>
</html>

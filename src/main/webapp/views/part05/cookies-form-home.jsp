<%-- 
    Document   : cookies-form-home
    Created on : Nov 2, 2021, 7:20:30 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"
	import="java.net.URLDecoder" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Developer Languages News</title>
    </head>
	
	<%
		String language = "Java";
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null && cookies.length > 0)
		{
			for (Cookie cookie : cookies)
			{
				if (cookie.getName().equals("app.language"))
				{
					language = URLDecoder.decode(cookie.getValue(), "UTF-8");
					break;
				}
			}
		}
	%>
	
    <body>
		<h1>Home page</h1>
		
        <h1><%= language %> news today.</h1>
		<% for (int i = 0; i < 3; i++) { %>
		<ul>
			<li><%= language %> blog <%= i %></li>
		</ul>
		<% } %>
		
		<h1><%= language %> recommend books.</h1>
		<% for (int i = 0; i < 3; i++) { %>
		<ul>
			<li><%= language %> book <%= i %></li>
		</ul>
		<% } %>
		
		<h1><%= language %> Jobs.</h1>
		<% for (int i = 0; i < 3; i++) { %>
		<ul>
			<li><%= language %> job <%= i %></li>
		</ul>
		<% } %>

		<a href="./cookies-form-request.jsp">Update your new favourite language:</a>
    </body>
</html>

<%-- 
    Document   : cookies-form-response
    Created on : Nov 2, 2021, 7:08:40 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"
	import="java.net.URLEncoder" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Developer Languages News</title>
		<style>
			.display-error
			{
				color: red;
			}
			
			.display-info
			{
				color: green;
			}
		</style>
    </head>
	
    <body>
		<%
			String language = request.getParameter("language");
			if (language != null && !language.equals(""))
			{
				String encodedlanguage = URLEncoder.encode(language, "UTF-8");
				Cookie languageCookie = new Cookie("app.language", encodedlanguage);
				languageCookie.setMaxAge(60 * 60 * 24 * 365);
				response.addCookie(languageCookie);
				%>
				<h1 class="display-info">Update <%= language %> as your new favourite language</h1>
				<%
			}
			else
			{
				%>
				<h1 class="display-error">No new favourite language :(</h1>
				<%
			}
		%>
		<a href="./cookies-form-home.jsp">Go to homepage</a>
    </body>
</html>

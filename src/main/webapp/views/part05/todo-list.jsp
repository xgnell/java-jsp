<%-- 
    Document   : todo-list
    Created on : Nov 2, 2021, 6:29:37 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"
	import="java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TODO List App</title>
    </head>
    <body>
        <h1>TODO List App</h1>
	
		<form method="POST" action="todo-list.jsp">
			Add todo: <input type="text" name="todo-task">
			<input type="submit" value="Add">
			<br>
			<input type="checkbox" name="is-todo-clear"> Clear todo list ?
		</form>
		
		<br>
		<hr>
		
		<h1>Todo list</h1>
		<%
			List<String> items = (List<String>) session.getAttribute("todo-items");
			if (items == null)
			{
				// Create todo-list
				items = new ArrayList<>();
				session.setAttribute("todo-items", items);
			}
			
			String[] isClear = request.getParameterValues("is-todo-clear");
			if (isClear != null && isClear.length > 0)
			{
				// Clear todo list
				items.clear();
			}
			else
			{
				// Get new task
				String task = request.getParameter("todo-task");
				if (task != null && !task.equals(""))
				{
					items.add(task);
				}
			}
			
			// Display
			for (String item : items)
			{
				%>
				<h2><%= item %></h2>
				<%
			}
			
		%>
    </body>
</html>

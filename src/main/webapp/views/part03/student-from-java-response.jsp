<%-- 
    Document   : student-from-java-response
    Created on : Oct 30, 2021, 7:45:19 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="blackghost.sample.jsp.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
		<style>
			.display-error {
				color: red;
			}
			
			.display-info {
				color: blue;
			}
		</style>
    </head>
    <body>
        <h1>Student Information</h1>
		<%
			String student_name = request.getParameter("student_name");
			String student_age = request.getParameter("student_age");
			if (student_name.equals("") || student_age.equals("")) {
				%>
				<h1 class="display-error">Missing student information :(</h1>
				<%
			} else {
				Student student = new Student(student_name, Integer.valueOf(student_age));
				%>
				<h1 class="display-info">Hello <%= student.getName() %>, you are <%= student.getAge() %> years old</h1>
				<%
			}
		%>
    </body>
</html>

<%-- 
    Document   : student-form-select-response
    Created on : Oct 30, 2021, 7:27:23 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student country</h1>
		<p>${param.student_name} comes from ${param.student_country}</p>
    </body>
</html>

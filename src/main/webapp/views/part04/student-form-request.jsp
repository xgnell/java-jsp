<%-- 
    Document   : form-data
    Created on : Oct 30, 2021, 6:39:15 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student Information</h1>
		<form method="GET" action="student-form-response.jsp">
			Name: <input type="text" name="student_name">
			<br>
			Age: <input type="number" name="student_age">
			<br>
			<input type="submit" value="Send">
		</form>
    </body>
</html>

<%-- 
    Document   : student-form-radio-response
    Created on : Oct 30, 2021, 7:33:00 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student favourite programming language</h1>
		<p>${param.student_name} likes ${param.student_language}</p>
    </body>
</html>

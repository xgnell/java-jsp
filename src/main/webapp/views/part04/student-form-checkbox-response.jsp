<%-- 
    Document   : student-form-checkbox-response
    Created on : Oct 30, 2021, 8:12:58 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
		<style>
			.display-info {
				color: blue;
			}
		</style>
    </head>
    <body>
        <h1>${param.student_name} favourite programming languages:</h1>
		<%
			String[] languages = request.getParameterValues("student_language");
			for (String language : languages) {
				%>
				<h2 class="display-info"><%= language %></h2>
				<%
			}
		%>
    </body>
</html>

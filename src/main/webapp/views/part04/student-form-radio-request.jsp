<%-- 
    Document   : student-form-radio-request
    Created on : Oct 30, 2021, 7:32:49 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student favourite programming language</h1>
		<form method="GET" action="student-form-radio-response.jsp">
			Name: <input type="text" name="student_name"><br>
			Language:<br>
			<input type="radio" name="student_language" value="C++">C++<br>
			<input type="radio" name="student_language" value="C#">C#<br>
			<input type="radio" name="student_language" value="Java">Java<br>
			<input type="radio" name="student_language" value="Python">Python<br>
			
			<br>
			<input type="submit" value="Send">
		</form>
    </body>
</html>

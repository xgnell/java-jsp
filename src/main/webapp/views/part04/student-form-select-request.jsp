<%-- 
    Document   : student-form-select-request
    Created on : Oct 30, 2021, 7:27:00 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student countries</h1>
		<form method="GET" action="student-form-select-response.jsp">
			Name: <input type="text" name="student_name"><br>
			Country: <select name="student_country">
				<option value="Vietnam">Vietname</option>
				<option value="America">America</option>
				<option value="Thailand">Thailand</option>
				<option value="China">China</option>
				<option value="Russia">Russia</option>
				<option value="Singapore">Singapore</option>
			</select><br>
			<input type="submit" value="Send">
		</form>
    </body>
</html>

<%-- 
    Document   : student-form-checkbox-request
    Created on : Oct 30, 2021, 8:12:42 PM
    Author     : blackghost
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Student favourite programming languages</h1>
		<form method="GET" action="student-form-checkbox-response.jsp">
			Name: <input type="text" name="student_name"><br>
			Language:<br>
			<input type="checkbox" name="student_language" value="C++">C++
			<input type="checkbox" name="student_language" value="PHP">PHP
			<input type="checkbox" name="student_language" value="Java">Java
			<input type="checkbox" name="student_language" value="Javascript">Javascript
			<br>
			<input type="submit" value="Send">
		</form>
    </body>
</html>
